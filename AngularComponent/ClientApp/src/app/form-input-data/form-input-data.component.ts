﻿import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-form-input-data',
  templateUrl: './form-input-data.component.html',
  styleUrls: ['./form-input-data.component.css']
})
export class FormInputDataComponent implements OnInit, OnChanges {
    student = {
        studentCode: null,
        studentName: null,
        studentAddress: null,
        studentPoint: null
    };
    checkShow = false;
    submitForm(): void {
        //if (this.student.studentCode == '' || this.student.studentName == '' || this.student.studentAddress == '' || this.student.studentPoint == '' || this.student.studentCode == null || this.student.studentName == null || this.student.studentAddress == null || this.student.studentPoint == null) {
        //    this.student.studentCode = '';
        //    this.student.studentName = '';
        //    this.student.studentAddress = '';
        //    this.student.studentPoint = '';
        //    return;
        //}
        this.checkShow = true;
        console.log(this.student);
    }
    OnInput(): void {
        this.checkShow = false;
    }
  constructor() { }
    ngOnChanges(changes: SimpleChanges): void {
        this.checkShow = false;
    }

    ngOnInit(): void {
        console.log(this.student);
  }

}
